from abc import ABC, abstractmethod


class BaseConnector(ABC):

    @abstractmethod
    async def test_connection(self):
        raise NotImplementedError

    @abstractmethod
    async def create_tables(self):
        raise NotImplementedError
