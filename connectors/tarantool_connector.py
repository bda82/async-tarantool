import logging

import tarantool

from config.config import Config

from . base_connector import BaseConnector


class TarantoolConnector(BaseConnector):
    """
    Tarantool Connector class
    """

    def __init__(self, space_name):
        super(TarantoolConnector, self).__init__()
        self._config = Config().tarantool
        self.space_name = space_name
        self.connection = None
        try:
            print(f'[+] Try connect tarantool')
            self.connection = tarantool.connect(host=self._config.host,
                                                port=self._config.port,
                                                user=self._config.user,
                                                password=self._config.password)
            print(f'[+] Tarantool connected')
        except Exception as ex:
            logging.error(f'[!] Tarantool connection error = {ex}')

    async def test_connection(self):
        pass

    async def create_tables(self):
        pass

