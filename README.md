# async-tarantool

### Первичная конфигурация для Tarantool

Конфигурация, точка соединения и основные операции для каждой сущности инкапсулированы в классах ConcreteModel(BaseModel) -> TarantoolConnector.

То есть вызов конкретной операции с данными Tarantool будет выглядеть следующим образом:

```
modelName.insert(data)
```

Каждая конкретная модель ConcreteModel должна определять имя Пространства Tarantool, с которым она работает как параметр: ```space_name = "concrete_space_name"```.

Это же самое имя должно определяться "со встречной стороны", то есть в самой модели LUA, загружаемой при старте Tarantool сервера.

С этим именем связаны операции на стороне Tarantool.

### Пример класса для работы с Tarantool

Возьмем пример кэша пользователей, посетивших наш сайт. Для этого определим базовую модель и наследование:

```
UsersModel(BaseModel) -> TarantoolConnector
```

Определим название пространства ```space_name = "users"``` в модели ```UsersModel``` для связи с внутренними операциями Tarantool.

Все модели собираются в python проекте в одно определенное место, то есть в ```models/tt/users_model```.

Соответственно, мы будем использовать определение модели, как:

```
from models.tt.users_model import UsersModel
users_model = UsersModel()
users_model.get_by_id(19)
```

### Разработка конфигурации для Tarantool

Tarantool запускается в соответствующем контейнере ```tarantool``` в сети контейнеров при помощи ```docker-compose```.

Для контейнера tarantool создан ```Dockerfile``` со следующим содержимым:

```
FROM tarantool/tarantool
COPY ./models/tt/lua_code/ /opt/tarantool/app.lua
CMD ["tarantool", "/opt/tarantool/app.lua"]
```
Из этого примера видно, что точкой запуска конфигурации tarantool является папка ```lua_code```, расположенная по пути ```./models/tt/lua_code/``` и основной точкой запуска является файл ```app.lua```, который монтируется в служебную директорию внутри контейнера ```/opt/tarantool/app.lua```. Он исполняется системным приложением ```tarantool`` и имеет соответствующий формат:

```
#!/usr/bin/env tarantool
print("Hello")
io.flush()
```

Lua поддерживает относительные импорты из других файлов, типа ```db = require("database.dbmodule")```, но для простоты мы пока будем использовать только один файл ```app.lua```.

### Разработка новых решений для работы с данными tarantool

Для разработки решений tarantool мы будем использовать lua, так как не все нужные нам функции работают с операциями по-умолчанию tarantool и нужно писать собственные функции. Кроме того, мы будем использовать структуризацию данных для моделей для формализации кода и операций с данными, а для этого также нужно работать с lua.

Итак, для начала работы, нам необходима точка доступа к конкретному экземпляру tarantool изнутри скрипта - нам это дает конструкция

```
box.cfg{listen = 3301}
```

После этого мы создадим объект Users = {} для определения структур данных и ассоциации методов.

Первым методом мы сделаем определение "таблицы" Users и ее создание:

```
function Users.users_create_database()
    local Model = {}
    ...
end
Users.users_create_database()
```

Здесь у нас объект Users будет создаваться при запуске ```app.lua``` при старте сервера tarantool.

Далее мы определим имя Пространства "users", такое же, как в python коде.

```
function Users.users_create_database()
    local Model = {}
    -- Space name
    Model.SPACE_NAME = "users"
    ...
end
```

После этого можно сделать именованные поля и индексы.

Первым действием определим саму схему для Пространства с форматом полей:

```
...
local userSpace = box.schema.space.create(Model.SPACE_NAME, {
    if_not_exists = true,
    format = {
        { name = 'id', type = 'unsigned'},
        { name = 'UserID', type = 'string' },
        { name = 'ViewID', type = 'string' },
        { name = 'VisitID', type = 'string' },
    }
})
...
```

Следует обратить внимание, что мы использовали флаг ```if_not_exists = true```, то есть схема создасться только если ее раньше не было. Если схему мы поменяли и ее нужно пересоздать заново - нам придется удалить контейнер и запустить скрипт заново.

После схемы, определим первичный индекс как ```hash``` для числового поля Model.ID:

```
...
userSpace:create_index(Model.PRIMARY_INDEX, {
    type = 'hash',
    parts = {Model.ID, 'unsigned'},
    if_not_exists = true
})
...
```

Затем можно определить другие индексы и параметры модели.

Для того, чтобы индекс для поля ```id``` работал как ```автоинкремент``` по аналогии с реляционной базой данных, мы должны определить ```последовательность userSeq``` и обращаться к ней каждый раз, когда мы хотим вставить новую запись в tarantool.

```
...
box.schema.sequence.create('userSeq', {
    if_not_exists = true,
    min = 1,
    step = 1
})
...
```

Для работы с последовательностью ```userSeq``` будем использовать функцию ```users_generate_next_id```, которая выглядит следующим образом:

```
function Users.users_generate_next_id()
    return box.sequence.userSeq:next()
end
```

После этого вставка новой записи Users будет выглядить следующим образом со стороны lua:

```
function users_insert(UserID, ViewID, VisitID)
    return box.space.users:insert({Users.users_generate_next_id(), UserID, ViewID, VisitID})
end
```

И таким образом со стороны python:

```    
def insert(self, data: tuple):
    if self.connection is not None:
        try:
            return self.connection.call('users_insert', (data[0], data[1], data[2]))
        except Exception as ex:
            print(f'[!] {ex}')
```

Обратите внимание, что здесь используется не операция tarantool ```insert```, а собственная функция ```users_insert```, куда передаются данные для вставки новой записи ```data```, разбитые в элементы кортэжа ```(data[0], data[1], data[2])```.

Примечание: Если нам нужно вызывать функции tarantool напрямую, мы используем прямой вызов с указанием имени Пространства: ```sel = self.connection.select(self.space_name, [])```, а если мы вызываем функцию с передачей параметров, мы используем прямой вызов: ```sel = self.connection.call('users_get_by_user_id', [str(user_id)])```

Рекомендую для создание новых объектов и функций tarantool, использовать именование, где на первой позиции ставится ```ИмяСущности```, а далее следует действие, например:

```
--
-- SITES Space (database)
--
...
local Sites = {}
...
function Sites.sites_create_database()
-- function body
end
...
Sites.sites_create_database()
...
function sites_get_by_user_id(id)
-- function body
end
...
```

При этом, ```box.cfg``` затрагивать не нужно.

### Использование Python обертки для Tarantool на примере UsersModel.

Так как мы определили модель данных для Пространства "users" в модели и инкапсулировали туда подключение к Tarantool, то там же для примера определим основные методы для работы с моделью данных, а именно:

1. is_connected - Признак успешного соединения (connection или None)
2. insert - вставка новой записи в Пространство из массива или контежа (вход data внутри разбирается на (data[0], data[1], data[2]))
3. select_all - выборка всех значений из Пространства
4. get_by_id - выборка по первичному индексу id (unsigned int, sequence)
5. select_by_user_id - выборка по UserID
6. delete_by_id - удаление записи по первичному индексу id (unsigned int, sequence)
7. delete_by_user_id - удаление по UserID
9. update - обновление записи по первичному индексу id (unsigned int, sequence)
10. update_user_id - обновление UserID по первичному индексу id (unsigned int, sequence)
11. update_visit_id - обновление VisitID по первичному индексу id (unsigned int, sequence)
12. update_view_id - обновление ViewID по первичному индексу id (unsigned int, sequence)

При необходимости возможно добавление других методов с созданием (или без создания) функций LUA в файле app.lua.