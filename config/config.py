import os


class PSQLConfig:
    host = ''
    port = ''
    user = ''
    password = ''
    db = ''


class RMQConfig:
    host = ''
    port = ''
    user = ''
    password = ''
    virtual_host = ''
    delivery_mode = ''
    prefetch_count = ''
    connector_queue = ''
    parser_queue = ''
    broker_queue = ''


class ClickHouseConfig:
    host = ''
    port = ''
    user = ''
    password = ''
    db = ''


class TarantoolConfig:
    host = ''
    port = ''
    user = ''
    password = ''
    slab_alloc_arena = 0.1



class DefaultParameters:
    default_partner_name = ''
    default_site_label = ''
    default_site_names_domain = ''
    default_category_types_name = ''


class CommandCodes:
    test = 'test'
    get_sites = 'get_sites'
    post_sites = 'post_sites'
    get_sites_site_id = 'get_sites_site_id'
    put_sites_site_id = 'put_sites_site_id'
    delete_sites_site_id = 'delete_sites_site_id'


class Config:

    def __init__(self):
        self.default_parameters = DefaultParameters()
        self.default_parameters.default_partner_name = 'Test'
        self.default_parameters.default_site_label = 'glas'
        self.default_parameters.default_site_names_domain = 'glas.ru'
        self.default_parameters.default_category_types_name = 'News'

        self.psql = PSQLConfig()
        self.psql.host = os.environ.get('POSTGRES_HOST', 'analytic_db')
        self.psql.port = os.environ.get('POSTGRES_PORT', 5432)
        self.psql.db = os.environ.get('POSTGRES_DB', 'analyticdb')
        self.psql.user = os.environ.get('POSTGRES_USER', 'root')
        self.psql.password = os.environ.get('POSTGRES_PASSWORD', 'password')

        self.rmq = RMQConfig()
        self.rmq.host = os.environ.get('RABBITMQ_HOST', 'rabbitmq')
        self.rmq.port = os.environ.get('RABBITMQ_PORT', 5672)
        self.rmq.user = os.environ.get('RABBITMQ_DEFAULT_USER', 'root')
        self.rmq.password = os.environ.get('RABBITMQ_DEFAULT_PASS', 'password')
        self.rmq.virtual_host = os.environ.get('RABBITMQ_DEFAULT_VIRTUAL_HOST', '/')
        self.rmq.delivery_mode = os.environ.get('RABBITMQ_DEFAULT_DELIVERY_MODE', 2)
        self.rmq.prefetch_count = os.environ.get('RABBITMQ_DEFAULT_PREFETCH_COUNT', 1)
        self.rmq.connector_queue = os.environ.get('RABBITMQ_CONNECTOR_QUEUE', 'connector_queue')
        self.rmq.parser_queue = os.environ.get('RABBITMQ_PARSER_QUEUE', 'parser_queue')
        self.rmq.broker_queue = os.environ.get('RABBITMQ_PARSER_QUEUE', 'broker_queue')

        self.clickhouse = ClickHouseConfig()
        self.clickhouse.host = os.environ.get('CLICKHOUSE_HOST', 'clickhouse_server')
        self.clickhouse.port = os.environ.get('CLICKHOUSE_PORT', 8123)
        self.clickhouse.db = os.environ.get('CLICKHOUSE_DB', 'clickhouse')
        self.clickhouse.user = os.environ.get('CLICKHOUSE_USER', 'root')
        self.clickhouse.password = os.environ.get('CLICKHOUSE_PASSWORD', 'password')

        self.command_codes = CommandCodes()

        self.tarantool = TarantoolConfig()
        self.tarantool.host = os.environ.get('TARANTOOL_HOST', 'localhost')
        self.tarantool.port = os.environ.get('TARANTOOL_PORT', 3301)
        self.tarantool.user = os.environ.get('TARANTOOL_USER_NAME', 'root')
        self.tarantool.password = os.environ.get('TARANTOOL_USER_PASSWORD', 'password')
        self.tarantool.slab_alloc_arena = os.environ.get('TARANTOOL_SLAB_ALLOC_ARENA', 0.1)
