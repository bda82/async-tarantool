#/bin/env bash
docker-compose down
docker rmi async-tarantool_tarantool --force
docker-compose up --build