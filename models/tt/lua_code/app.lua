#!/usr/bin/env tarantool

--
-- BOX Connection point
--

box.cfg{listen = 3301}

--
-- Common functions
--

function log(s)
    print(s)
    io.flush()
end

--
-- USERS Space (database)
--

local Users = {}

function Users.users_create_database()
    local Model = {}
    -- Space name
    Model.SPACE_NAME = "users"
    -- Index names
    Model.PRIMARY_INDEX = "primary"
    Model.USERID_INDEX = 'secondary'
    -- Field numbers in data tuple
    Model.ID = 1
    Model.UserID = 2
    Model.ViewID = 3
    Model.VisitID = 4

    local userSpace = box.schema.space.create(Model.SPACE_NAME, {
        if_not_exists = true,
        format = {
            { name = 'id', type = 'unsigned'},
            { name = 'UserID', type = 'string' },
            { name = 'ViewID', type = 'string' },
            { name = 'VisitID', type = 'string' },
        }
    })

    userSpace:create_index(Model.PRIMARY_INDEX, {
        type = 'hash',
        parts = {Model.ID, 'unsigned'},
        if_not_exists = true
    })

    userSpace:create_index(Model.USERID_INDEX, {
        type = 'tree',
        parts = {Model.UserID, 'string'},
        unique = true,
        if_not_exists = true
    })

    box.schema.sequence.create('userSeq', {
        if_not_exists = true,
        min = 1,
        step = 1
    })
end

function Users.users_generate_next_id()
    return box.sequence.userSeq:next()
end

function users_insert(UserID, ViewID, VisitID)
    log('Insert UserID = ' .. UserID .. ', ViewID = ' .. ViewID .. ', VisitID = ' .. VisitID)
    return box.space.users:insert({Users.users_generate_next_id(), UserID, ViewID, VisitID})
end

function users_get_by_user_id(id)
    log('Get select request, id = ' .. id)
    return box.space.users.index.secondary:select({id}, {iterator = box.index.EQ})
end

function users_delete_by_user_id(UserID)
    log('Get delete request, id = ' .. UserID)
    return box.space.users.index.secondary:delete(UserID)
end

function users_update_all(id, UserID, ViewID, VisitID)
    log('Update fields UserID = ' .. UserID .. ', ViewID = ' .. ViewID .. ', VisitID = ' .. VisitID)
    return box.space.users:update(id, {{'=', 2, UserID}, {'=', 3, ViewID}, {'=', 4, VisitID}})
end

function users_update_user_id(id, UserID)
    log('Update UserID = ' .. UserID)
    return box.space.users:update(id, {{'=', 2, UserID}})
end

function users_update_view_id(id, ViewID)
    log('Update ViewID = ' .. ViewID)
    return box.space.users:update(id, {{'=', 3, ViewID}})
end

function users_update_visit_id(id, VisitID)
    log('Update VisitID = ' .. VisitID)
    return box.space.users:update(id, {{'=', 4, VisitID}})
end

Users.users_create_database()

--
--
--