from . base_model import BaseModel

from connectors.tarantool_connector import TarantoolConnector


class UsersModel(BaseModel):

    space_name = "users"
    connector = None

    def __init__(self):
        super().__init__()
        print(f'[+] Init model with space_name = {self.space_name}')
        self.connector = TarantoolConnector(self.space_name)

    @property
    def is_connected(self):
        """
        If connector if open or closed
        """
        print(f'[+] Tarantool connector: {self.connector}')
        res = self.connector is None
        print(f'[+] Connection is None = {res}')
        if not res:
            print(f'[+] Connection is OK')
            return True
        print(f'[!] Connection is None')
        return False

    def insert(self, data: tuple):
        if self.connector is not None:
            try:
                print(f'[+] Try to insert data = {data}')
                res = self.connector.connection.call('users_insert', (data[0], data[1], data[2]))
                print(f'[+] insertion result = ', res)
                return res
            except Exception as ex:
                print(f'[!] {ex}')
                return
        print(f'[!] No Tarantool Connection')

    def select_all(self):
        if self.is_connected:
            print(f'Select all users')
            sel = self.connector.connection.select(self.space_name, [])
            print(f'[+] Selection result = {sel}')
            # needs to map all tuple data
            # [+] Selection result =
            #     - [19, '1', '2', '3']
            #     - [27, '0', '1', '2']
            #     - [28, '10', '11', '12']
            #     - [29, '0026059e-8030-419f-8e12-b0c3c1e60d68', '0026059e-8030-419f-8e12-b0c3c1e60d72', '0026059e-8030-419f-8e12-b0c3c1e60d73']
            return sel

    def get_by_id(self, id):
        if self.is_connected:
            print(f'[+] ID = {id}')
            sel = self.connector.connection.select(self.space_name, [id])
            print(f'[+] Selection result = {sel}')
            # [+] Selection result =
            #     - [19, '1', '2', '3']
            return sel

    def select_by_user_id(self, user_id):
        if self.is_connected:
            print(f'[+] UserID = {user_id}')
            sel = self.connector.connection.call('users_get_by_user_id', [str(user_id)])
            print(f'[+] Selection result = {sel}')
            # [+] Selection result =
            #     - [[29, '0026059e-8030-419f-8e12-b0c3c1e60d68', '0026059e-8030-419f-8e12-b0c3c1e60d72', '0026059e-8030-419f-8e12-b0c3c1e60d73']]
            return sel

    def delete_by_id(self, id):
        if self.is_connected:
            print(f'ID = {id}')
            dl = self.connector.connection.delete(self.space_name, [id])
            print(f'[+] Deletion result = {dl}')
            return dl

    def delete_by_user_id(self, user_id):
        if self.is_connected:
            print(f'UserID = {id}')
            dl = self.connector.connection.call('users_delete_by_user_id', [user_id])
            print(f'[+] Deletion result = {dl}')
            return dl

    def update(self, id, data: tuple):
        if self.is_connected:
            print(f'ID = {id}, data = {data}')
            upd = self.connector.connection.call('users_update_all', (int(id), data[0], data[1], data[2]))
            print(f'[+] Update result = {upd}')
            return upd

    def update_user_id(self, id, user_id):
        if self.is_connected:
            print(f'ID = {id}, user_id = {user_id}')
            upd = self.connector.connection.call('users_update_user_id', (int(id), user_id))
            print(f'[+] Update result = {upd}')
            return upd

    def update_visit_id(self, id, visit_id):
        if self.is_connected:
            print(f'ID = {id}, visit_id = {visit_id}')
            upd = self.connector.connection.call('users_update_visit_id', (int(id), visit_id))
            print(f'[+] Update result = {upd}')
            return upd

    def update_view_id(self, id, view_id):
        if self.is_connected:
            print(f'ID = {id}, view_id = {view_id}')
            upd = self.connector.connection.call('users_update_view_id', (int(id), view_id))
            print(f'[+] Update result = {upd}')
            return upd