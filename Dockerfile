FROM tarantool/tarantool

COPY ./models/tt/lua_code/ /opt/tarantool/app.lua

CMD ["tarantool", "/opt/tarantool/app.lua"]